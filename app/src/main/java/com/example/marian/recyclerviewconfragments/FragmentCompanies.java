    package com.example.marian.recyclerviewconfragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.marian.recyclerviewconfragments.recyclerView.CompanyAdapter;
import com.example.marian.recyclerviewconfragments.recyclerView.CompanyCard;
import com.example.marian.recyclerviewconfragments.recyclerView.ItemOffsetDecoration;

import java.util.ArrayList;
import java.util.Arrays;

public class FragmentCompanies extends Fragment {

    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    private ArrayList logos=new ArrayList<>(Arrays.asList(R.drawable.aschen_ico,R.drawable.aschen_ico,R.drawable.astillero_ico,R.drawable.audio_ad_ico,
            R.drawable.celerative_ico,R.drawable.chulengo_ico,R.drawable.connectar_ico,R.drawable.cor_ico,R.drawable.croma_ico,
            R.drawable.dev_artis_ico,R.drawable.entropeer_ico,R.drawable.essentit_ico,R.drawable.etermax_ico,R.drawable.genosha_ico,
            R.drawable.gyl_group_ico, R.drawable.havas_ico,R.drawable.redmas_ico,R.drawable.latilingua_ico,R.drawable.mind_cotine_ico,
            R.drawable.pencillus_ico, R.drawable.rock_city_ico,R.drawable.syur_ico,R.drawable.taringa_ico,R.drawable.tisa_ico,
            R.drawable.ushowapp_ico,R.drawable.u_sound_ico,R.drawable.welo_ico,R.drawable.wolox_ico));

    private ArrayList coloreFondosComNames=new ArrayList<>(Arrays.asList(R.color.lima,R.color.gold,R.color.lima,R.color.gold,R.color.lima,
                R.color.gold,R.color.lima,R.color.gold,R.color.lima,R.color.gold,R.color.lima,R.color.gold,R.color.lima,R.color.gold,R.color.lima,
                R.color.gold,R.color.lima,R.color.gold,R.color.lima,R.color.gold,R.color.lima,R.color.gold,R.color.lima,R.color.gold,R.color.lima,
                R.color.gold,R.color.lima,R.color.gold));


    private ArrayList<CompanyCard> companiesCards=new ArrayList<>();

    private OnFragmentInteractionListener mListener;


    public FragmentCompanies() {
        // Required empty public constructor
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View v=inflater.inflate(R.layout.fragment_companies,container,false);
        recyclerView=(RecyclerView) v.findViewById(R.id.recyclerViewCompanies);

        recyclerView.setHasFixedSize(true);

        layoutManager=new GridLayoutManager(getActivity(),3);

        recyclerView.setLayoutManager(layoutManager);

        ItemOffsetDecoration itemDecoration=new ItemOffsetDecoration(getContext(),R.dimen.margin_gridview);

        recyclerView.addItemDecoration(itemDecoration);
        cargarDatos();



        adapter=new CompanyAdapter(getContext(),companiesCards);

        recyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();

        return v;
    }



    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }



        private void cargarDatos() {



            //  int cant=getResources().getStringArray(R.array.CompanyNames).length;
            int cant=26;




            String[] namesArr=getResources().getStringArray(R.array.CompanyNames);
            String[] whoWeAreArr=getResources().getStringArray(R.array.WhoWeAre);
            String[] emailsArr=getResources().getStringArray(R.array.Emails);
            String[] phonesArr=getResources().getStringArray(R.array.Phones);
            String[] backColorCompNamesArr=getResources().getStringArray(R.array.BackColorsCompNames);

            for(int i=0;i<cant;i++) {


                CompanyCard cCard=new CompanyCard(((int) logos.get(i)), (String) namesArr[i],
                        (String) whoWeAreArr[i], (String) emailsArr[i], (String) phonesArr[i],(int) coloreFondosComNames.get(i));

                companiesCards.add(cCard);

            }
        }







}

package com.example.marian.recyclerviewconfragments;



import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.TypedValue;

import com.example.marian.recyclerviewconfragments.recyclerView.CompanyAdapter;
import com.example.marian.recyclerviewconfragments.recyclerView.CompanyCard;

import java.util.ArrayList;
import java.util.Arrays;

public class MainActivity extends AppCompatActivity implements FragmentCompanies.OnFragmentInteractionListener{

    /*private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    private ArrayList logos=new ArrayList<>(Arrays.asList(

            R.drawable.lmate_ico,R.drawable.aschen_ico,R.drawable.astillero_ico,R.drawable.audioad_ico,R.drawable.celerative_ico,R.drawable.chulengo_ico,
            R.drawable.connectar_ico,R.drawable.cor_ico,R.drawable.croma_ico,R.drawable.devartis_ico,R.drawable.entropeer_ico,R.drawable.essentit_ico,R.drawable.etermax_ico,
            R.drawable.genosha_ico,R.drawable.gyl_ico,R.drawable.gyl_ico,R.drawable.gyl_ico,R.drawable.latinlingua_ico,R.drawable.mindcotine_ico,
            R.drawable.pencillus_ico,R.drawable.rockcity_ico,R.drawable.syur_ico,R.drawable.taringa_ico,R.drawable.tisa_ico,R.drawable.tisa_ico,R.drawable.usound_ico,
            R.drawable.welo_ico,R.drawable.wolox_ico));

    private ArrayList coloreFondosComNames=new ArrayList<>(Arrays.asList(R.color.lima,R.color.gold,R.color.lima,R.color.gold,R.color.lima,
            R.color.gold,R.color.lima,R.color.gold,R.color.lima,R.color.gold,R.color.lima,R.color.gold,R.color.lima,R.color.gold,R.color.lima,
            R.color.gold,R.color.lima,R.color.gold,R.color.lima,R.color.gold,R.color.lima,R.color.gold,R.color.lima,R.color.gold,R.color.lima,
            R.color.gold,R.color.lima,R.color.gold));


    private ArrayList<CompanyCard> companiesCards=new ArrayList<>();


*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
       /* recyclerView=(RecyclerView) findViewById(R.id.recyclerViewCompanies);
        recyclerView.setHasFixedSize(true);
        // layoutManager=new GridLayoutManager(this,getSpanCount() );
        layoutManager=new GridLayoutManager(this,3);
        recyclerView.setLayoutManager(layoutManager);
        cargarDatos();
        adapter=new CompanyAdapter(this,companiesCards);
        recyclerView.setAdapter(adapter); */


       //FragmentCompanies fragCompanies=new FragmentCompanies();


    }



    /*private void cargarDatos() {

        //  int cant=getResources().getStringArray(R.array.CompanyNames).length;
        int cant=6;

        String[] namesArr=getResources().getStringArray(R.array.CompanyNames);
        String[] whoWeAreArr=getResources().getStringArray(R.array.WhoWeAre);
        String[] emailsArr=getResources().getStringArray(R.array.Emails);
        String[] phonesArr=getResources().getStringArray(R.array.Phones);
        //String[] backColorCompNamesArr=getResources().getStringArray(R.array.BackColorsCompNames);

        for(int i=0;i<cant;i++) {


            CompanyCard cCard=new CompanyCard(((int) logos.get(i)), (String) namesArr[i],
                    (String) whoWeAreArr[i], (String) emailsArr[i], (String) phonesArr[i],(int) coloreFondosComNames.get(i));

            companiesCards.add(cCard);

        }
    }
*/

    public int getSpanCount()
    {
        DisplayMetrics displayMetrics=new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int anchuraPantalla=displayMetrics.widthPixels;
        float anchuraMinimaElemento= TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,75,displayMetrics);
        return (int) (anchuraPantalla/anchuraMinimaElemento);
    }


    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}


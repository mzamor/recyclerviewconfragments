package com.example.marian.recyclerviewconfragments.recyclerView;

import android.app.Dialog;
import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.marian.recyclerviewconfragments.R;

import java.util.ArrayList;

/**
 * Created by marian on 28/01/18.
 */

public class CompanyAdapter extends RecyclerView.Adapter<CompanyViewHolder> {

    private ArrayList<CompanyCard> companiesCards;
    private Context context;




  public CompanyAdapter(Context contextA, ArrayList companiesCard) {

       this.context=contextA;
       this.companiesCards=companiesCard;



   }

    @Override
    public CompanyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater inflater=LayoutInflater.from(context);
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_recycler_company,null);
        CompanyViewHolder compVH= new CompanyViewHolder(view);
        return compVH;
    }

    @Override
    public void onBindViewHolder(CompanyViewHolder holder, int position) {

        holder.companyName.setText((CharSequence) companiesCards.get(position).getCompanyName());

        // ver el tema de colores de fondo. A cada tarjeta habra que cargarle un color. En values/colors hacer una lista de colores
        // que elija y luego le creo el atributo color fondo mas al objeto card

        holder.companyName.setBackgroundColor(ContextCompat.getColor(context,companiesCards.get(position).getIdColorFondoName()));
        holder.logoHolder.setImageResource((Integer) companiesCards.get(position).getIdLogo());
        holder.containerHolder.setOnClickListener(OnClickListener(position));
    }



    private void setDataToView(ImageView companyLogo,TextView companyWhoWeAre,TextView companyEmail,TextView companyPhone, int position) {

        companyLogo.setImageResource((Integer) companiesCards.get(position).getIdLogo());
        companyWhoWeAre.setText((CharSequence) companiesCards.get(position).getWhoWeAre());
        companyEmail.setText((CharSequence) companiesCards.get(position).getEmail());
        companyPhone.setText((CharSequence)companiesCards.get(position).getTelefono());
    }



    private View.OnClickListener OnClickListener(final int position) {
        return new View.OnClickListener(){
            @Override
            public void onClick(View view) {
               final Dialog dialog=new Dialog(context);
                dialog.setContentView(R.layout.item_company_selected);
                dialog.setTitle((CharSequence) companiesCards.get(position).getCompanyName());
                dialog.setCancelable(true);
                ImageView logoCardV=(ImageView) dialog.findViewById(R.id.logoCV_Sel);
                TextView whoWeAreN=(TextView)dialog.findViewById(R.id.whoWeAreCV_Sel);
                TextView emailN=(TextView)dialog.findViewById(R.id.email_Sel);
                TextView phoneN=(TextView)dialog.findViewById(R.id.phone_Sel);
                setDataToView(logoCardV,whoWeAreN,emailN,phoneN,position);
                dialog.show();
            }

        };
    }



    @Override
    public int getItemCount() {

        //return companiesCards.size();
        return 26;
    }
}

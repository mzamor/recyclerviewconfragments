package com.example.marian.recyclerviewconfragments.recyclerView;

/**
 * Created by marian on 04/02/18.
 */

public class CompanyCard {

    int idLogo;
    String companyName;
    String whoWeAre;
    String email;
    String telefono;
    int idColorFondoName;


    public CompanyCard(int idLogo, String companyName, String whoWeAre, String email, String telefono, int idColorBackName) {
        this.idLogo = idLogo;
        this.companyName = companyName;
        this.whoWeAre = whoWeAre;
        this.email = email;
        this.telefono = telefono;
        this.idColorFondoName=idColorBackName;
    }



    public int getIdLogo() {
        return idLogo;
    }

    public void setIdLogo(int idLogo) {
        this.idLogo = idLogo;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getWhoWeAre() {
        return whoWeAre;
    }

    public void setWhoWeAre(String whoWeAre) {
        this.whoWeAre = whoWeAre;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public int getIdColorFondoName() {
        return idColorFondoName;
    }

    public void setIdColorFondoName(int idColorFondoName) {
        this.idColorFondoName = idColorFondoName;
    }


}

package com.example.marian.recyclerviewconfragments.recyclerView;

import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.marian.recyclerviewconfragments.R;

/**
 * Created by marian on 28/01/18.
 */

public class CompanyViewHolder extends ViewHolder {


    public TextView companyName;
    public ImageView logoHolder;
    public View containerHolder;


    public CompanyViewHolder(View itemView) {
        super(itemView);

        companyName = (TextView) itemView.findViewById(R.id.companyNameCV);
        logoHolder = (ImageView) itemView.findViewById(R.id.logoCV);
        containerHolder = itemView.findViewById(R.id.card_view_company);
    }


  public TextView getCompanyName() {         return companyName;    }


    public ImageView getLogo() {
        return logoHolder;
    }

    public View getContainer() {
        return containerHolder;
    }

}